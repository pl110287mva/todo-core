﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.DataAccess.Models;

namespace ToDo.Services.Interfaces
{
    public interface ITaskService
    {
        // TODO comments
        Task<IEnumerable<AppTask>> GetAllTasksAsync(string type);
        Task<AppTask> GetTaskAsync(int taskId);
        Task<int> CreateTaskAsync(AppTask newTask);
        Task UpdateTaskAsync(AppTask updatedTask);
        Task DeleteTaskAsync(int taskId);
        Task ChangeCompletedStatusAsync(int taskId);
        Task DeleteAllCompletedTasksAsync();
    }
}

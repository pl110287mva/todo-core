﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.DataAccess;
using ToDo.DataAccess.Models;
using ToDo.Services.Interfaces;

namespace ToDo.Services
{
    public class TaskService : ITaskService
    {
        public const string CompleteStatus = "complete";
        public const string ActiveStatus = "active";

        private readonly ToDoContext dbContext;

        public TaskService(ToDoContext dbContext)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        /// <summary>
        /// Get all tasks from db.
        /// </summary>
        /// <returns>tasks collection</returns>
        public async Task<IEnumerable<AppTask>> GetAllTasksAsync(string type)
        {
            //IEnumerable<AppTask> tasks;
            var query = this.dbContext
                .Tasks
                .Where(t => t.IsDeleted == false)                
                .AsQueryable();
            
            query = this.CheckNeedForSorting(query, type);

            return await query
                .AsNoTracking()
                .ToListAsync();
        }

        /// <summary>
        /// Get task info from db.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>task information</returns>
        public async Task<AppTask> GetTaskAsync(int id)
        {
            return await this.dbContext
                .Tasks
                .Where(t => t.IsDeleted == false)
                .AsNoTracking()
                .FirstAsync(t => t.Id == id);
        }

        /// <summary>
        /// Add new tsk to db.
        /// </summary>
        /// <param name="newTask"></param>
        /// <returns>created task id</returns>
        public async Task<int> CreateTaskAsync(AppTask newTask)
        {
            await this.dbContext.Tasks.AddAsync(newTask);
            await this.dbContext.SaveChangesAsync();
            return newTask.Id;
        }

        /// <summary>
        /// Update task information in db.
        /// </summary>
        /// <param name="updatedTask"></param>
        /// <returns></returns>
        public async Task UpdateTaskAsync(AppTask updatedTask)
        {
            var task = await this.GetTaskAsync(updatedTask.Id);
            task.Description = updatedTask.Description;
            this.dbContext.Tasks.Update(task);
            await this.dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Remove task from db(softdelete).
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public async Task DeleteTaskAsync(int taskId)
        {
            var task = await this.GetTaskAsync(taskId);
            task.IsDeleted = true;
            task.DeletedAt = DateTime.UtcNow;
            this.dbContext.Tasks.Update(task);
            await this.dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Change task status.
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public async Task ChangeCompletedStatusAsync(int taskId)
        {
            var task = await this.GetTaskAsync(taskId);
            task.IsCompleted = !task.IsCompleted;
            this.dbContext.Tasks.Update(task);
            await this.dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Delete all completed tasks.
        /// </summary>
        /// <returns></returns>
        public async Task DeleteAllCompletedTasksAsync()
        {
            var currentDate = DateTime.UtcNow;
            var tasks = await this.GetAllTasksAsync(CompleteStatus);

            var updatedTasks = tasks
                .Select(t => { t.IsDeleted = true; t.DeletedAt = currentDate; return t; })
                .ToList();

            this.dbContext.UpdateRange(tasks);
            await this.dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Adding conditions to the request depending on the needs.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        private IQueryable<AppTask> CheckNeedForSorting(IQueryable<AppTask> query, string type)
        {
            if(type != null)
            {
                if (type == CompleteStatus)
                {
                    query = query.Where(t => t.IsCompleted == true);
                }
                else if (type == ActiveStatus)
                {
                    query = query.Where(t => t.IsCompleted == false);
                }
            }
            return query;
        }
    }
}

﻿using AutoMapper;
using ToDo.Controllers.RequestModels;
using ToDo.Controllers.ResponseModels;
using ToDo.DataAccess.Models;

namespace ToDo.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // from request
            CreateMap<TaskCreateRequest, AppTask>();
            CreateMap<TaskUpdateRequest, AppTask>();

            // to response
            CreateMap<AppTask, TaskResponse>();
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace ToDo.Controllers.RequestModels
{
    /// <summary>
    /// Request model for creating task.
    /// </summary>
    public class TaskCreateRequest
    {
        /// <summary>
        /// Task text.
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string Description { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace ToDo.Controllers.RequestModels
{
    /// <summary>
    /// Request model for updating task.
    /// </summary>
    public class TaskUpdateRequest : TaskCreateRequest
    {    
        /// <summary>
        /// Task id.
        /// </summary>
        public int Id { get; set; }
    }
}

﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDo.Controllers.Filters;
using ToDo.Controllers.RequestModels;
using ToDo.Controllers.ResponseModels;
using ToDo.DataAccess.Models;
using ToDo.Services.Interfaces;

namespace ToDo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly ITaskService taskService;

        public TasksController(IMapper mapper, ITaskService taskService)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.taskService = taskService ?? throw new ArgumentNullException(nameof(taskService));
        }

        /// <summary>
        /// Get all tasks.
        /// </summary>
        /// <param name="type">get tasks according to active or complete status</param>
        /// <returns>tasks collection according to type</returns>
        [HttpGet]
        [TaskTypesValidate]
        public async Task<IActionResult> GetAll([FromQuery] string type)
        {
            var tasks = await this.taskService.GetAllTasksAsync(type);
            var mappedTasks = this.mapper.Map<IEnumerable<TaskResponse>>(tasks);
            return Ok(mappedTasks);
        }

        /// <summary>
        /// Add new task to db.
        /// </summary>
        /// <param name="request">TaskRequestModel</param>
        /// <returns>create result</returns>
        [HttpPost]
        [ValidateModelState]
        public async Task<IActionResult> Add([FromBody] TaskCreateRequest request)
        {
            var mappedTask = this.mapper.Map<AppTask>(request);
            var id = await this.taskService.CreateTaskAsync(mappedTask);
            return CreatedAtAction(nameof(Update), new { id }, null);
        }

        /// <summary>
        /// Update task information in db.
        /// </summary>
        /// <param name="id">task id</param>
        /// <param name="request">TaskUpdateRequest model</param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [ValidateModelState]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] TaskUpdateRequest request)
        {
            request.Id = id;
            var mappedTask = this.mapper.Map<AppTask>(request);
            await this.taskService.UpdateTaskAsync(mappedTask);
            return NoContent();
        }

        /// <summary>
        /// Delete task from db.
        /// </summary>
        /// <param name="id">task id</param>
        /// <returns>status code</returns>
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            await this.taskService.DeleteTaskAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Change task status active or complete.
        /// </summary>
        /// <param name="id">task id whose status need to change</param>
        /// <returns>status code</returns>
        [HttpPut("{id:int}/status")]
        public async Task<IActionResult> ChangeStatus([FromRoute] int id)
        {
            await this.taskService.ChangeCompletedStatusAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Delete all completed tasks
        /// </summary>
        /// <returns>status code</returns>
        [HttpDelete("completed")]
        public async Task<IActionResult> ClearCompleted()
        {
            await this.taskService.DeleteAllCompletedTasksAsync();
            return NoContent();
        }
    }
}

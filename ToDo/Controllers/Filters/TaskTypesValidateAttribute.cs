﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.Services;

namespace ToDo.Controllers.Filters
{
    public class TaskTypesValidateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if(context.HttpContext.Request.Query.ContainsKey("type"))
            {
                string type = context.HttpContext.Request.Query["type"];
                if(type == TaskService.ActiveStatus || type == TaskService.CompleteStatus)
                {
                    return;
                }
                context.Result = new BadRequestResult();
            }
        }
    }
}

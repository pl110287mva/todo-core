﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using ToDo.DataAccess.Configurations;
using ToDo.DataAccess.Models;
using ToDo.DataAccess.Seeds;

namespace ToDo.DataAccess
{
    public class ToDoContext : DbContext
    {
        public ToDoContext(DbContextOptions<ToDoContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TaskConfiguration());
            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();
        }

        public DbSet<AppTask> Tasks { get; set; }

        /// <summary>
        /// Override base method, added dates generator.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> SaveChangesAsync(CancellationToken? cancellationToken = null)
        {
            foreach (var entry in this.ChangeTracker.Entries<IEntity>())
            {
                var currentDate = DateTime.UtcNow;

                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedAt = currentDate;
                        entry.Entity.UpdatedAt = currentDate;
                        break;

                    case EntityState.Modified:
                        entry.Entity.UpdatedAt = currentDate;
                        break;

                    default:
                        break;
                }
            }

            return await (cancellationToken.HasValue
                ? base.SaveChangesAsync(cancellationToken.Value)
                : base.SaveChangesAsync());
        }
    }
}

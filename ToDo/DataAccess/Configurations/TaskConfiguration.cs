﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDo.DataAccess.Models;

namespace ToDo.DataAccess.Configurations
{
    public class TaskConfiguration : IEntityTypeConfiguration<AppTask>
    {
        public void Configure(EntityTypeBuilder<AppTask> builder)
        {
            builder.ToTable("Tasks");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Description).IsRequired().HasMaxLength(255);
            builder.Property(t => t.IsCompleted).HasDefaultValue(false);
            builder.Property(t => t.IsDeleted).HasDefaultValue(false);
            builder.Property(t => t.CreatedAt).Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;
        }
    }
}

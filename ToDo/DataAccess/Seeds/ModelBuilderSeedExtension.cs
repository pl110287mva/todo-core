﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using ToDo.DataAccess.Models;

namespace ToDo.DataAccess.Seeds
{
    public static class ModelBuilderSeedExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppTask>().HasData(TestDataCollections.Tasks.ToArray());            
        }
    }
}

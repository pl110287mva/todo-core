﻿using System;
using System.Collections.Generic;
using ToDo.DataAccess.Models;

namespace ToDo.DataAccess.Seeds
{
    public static class TestDataCollections
    {
        /// <summary>
        /// AppTask test collection.
        /// </summary>
        public static IEnumerable<AppTask> Tasks { get; set; }

        static TestDataCollections()
        {
            var currentTime = DateTime.UtcNow;

            Tasks = new List<AppTask>
            {
                new AppTask
                {
                    Id = 1001,
                    Description = "First task",
                    CreatedAt = currentTime,
                    UpdatedAt = currentTime
                },
                new AppTask
                {
                    Id = 1002,
                    Description = "Secont task",
                    CreatedAt = currentTime,
                    UpdatedAt = currentTime
                },
                new AppTask
                {
                    Id = 1003,
                    Description = "First completed task",
                    CreatedAt = currentTime,
                    UpdatedAt = currentTime,
                    IsCompleted = true
                },
                new AppTask
                {
                    Id = 1004,
                    Description = "Second completed task",
                    CreatedAt = currentTime,
                    UpdatedAt = currentTime,
                    IsCompleted = true
                },
                new AppTask
                {
                    Id = 1005,
                    Description = "Deleted task",
                    CreatedAt = currentTime,
                    UpdatedAt = currentTime,
                    DeletedAt = currentTime,
                    IsDeleted = true                    
                }
            };
        }
    }
}

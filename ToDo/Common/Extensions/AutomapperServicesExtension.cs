﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ToDo.Helpers;

namespace ToDo.Common.Extensions
{
    public static class AutomapperServicesExtension
    {
        /// <summary>
        /// Startup configure services extension for Automapper.
        /// </summary>
        /// <param name="services">ServiceCollection</param>
        public static void AddAutomapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}

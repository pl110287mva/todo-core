﻿using Microsoft.Extensions.DependencyInjection;
using ToDo.Services;
using ToDo.Services.Interfaces;

namespace ToDo.Common.Extensions
{
    public static class DIServicesExtension
    {
        /// <summary>
        /// Service collection extension for DI services.
        /// </summary>
        /// <param name="services">service collection</param>
        /// <returns>updated service collection</returns>
        public static IServiceCollection AddAppServices(this IServiceCollection services)
        {
            services.AddScoped<ITaskService, TaskService>();
            
            return services;
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDo.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 255, nullable: false),
                    IsCompleted = table.Column<bool>(nullable: false, defaultValue: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "Description", "IsCompleted", "IsDeleted", "UpdatedAt" },
                values: new object[,]
                {
                    { 1001, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc), null, "First task", false, false, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc) },
                    { 1002, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc), null, "Secont task", false, false, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc) },
                    { 1003, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc), null, "First completed task", true, false, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc) },
                    { 1004, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc), null, "Second completed task", true, false, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc) },
                    { 1005, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc), new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc), "Deleted task", false, true, new DateTime(2019, 8, 2, 19, 17, 43, 731, DateTimeKind.Utc) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");
        }
    }
}

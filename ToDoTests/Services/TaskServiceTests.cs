﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.DataAccess.Models;
using ToDo.DataAccess.Seeds;
using ToDo.Services;
using ToDo.Services.Interfaces;

namespace ToDoTests
{
    [TestClass]
    public class TaskServiceTests
    {
        private ITaskService taskService;
        private IEnumerable<AppTask> tasksCollection;

        [TestInitialize]
        public void TestInitialize()
        {
            this.tasksCollection = TestDataCollections.Tasks;

            var contextBuilder = new TestDbContextBuilder();
            contextBuilder.FillDb(tasksCollection);
            var context = contextBuilder.BuildContext();
            
            this.taskService = new TaskService(context);
        }

        [TestMethod]
        public async Task GetTaskAsync_ExpectedNotNullObject()
        {
            // Arrange
            var expectedTask = this.tasksCollection.Where(t => t.IsDeleted != true).First();

            // Act
            var result = await this.taskService.GetTaskAsync(expectedTask.Id);

            // Assert
            Assert.IsNotNull(result, $"Task with {expectedTask.Id} not found");
            Assert.AreEqual(expectedTask.Id, result.Id);
        }

        [TestMethod]
        public async Task CreateTaskAsync_ExpectedNotNullId()
        {
            // Arrange
            var testTask = new AppTask { Description = "Test description"};

            // Act
            await this.taskService.CreateTaskAsync(testTask);
            
            // Assert            
            Assert.IsNotNull(testTask.Id, $"Task Id is null");
        }

        [TestMethod]
        public async Task GetAllTasksAsync_Expected4TasksInDb()
        {
            // Arrange
            var expectedTasks = this.tasksCollection.Where(t => t.IsDeleted == false).Count();

            // Act
            var tasks = await this.taskService.GetAllTasksAsync(null);

            // Assert            
            Assert.AreEqual(tasks.Count(), expectedTasks);
        }

        [TestMethod]
        public async Task GetAllTasksAsync_Expected2CompletedTasksInDb()
        {
            // Arrange
            var expectedTasks = this.tasksCollection.Where(t => t.IsCompleted == true).Count();

            // Act
            var tasks = await this.taskService.GetAllTasksAsync(TaskService.CompleteStatus);

            // Assert            
            Assert.AreEqual(tasks.Count(), expectedTasks);
        }

        [TestMethod]
        public async Task GetAllTasksAsync_Expected2ActiveTasksInDb()
        {
            // Arrange
            var expectedTasks = this.tasksCollection
                .Where(t => t.IsCompleted == false && t.IsDeleted == false).Count();

            // Act
            var tasks = await this.taskService.GetAllTasksAsync(TaskService.ActiveStatus);

            // Assert            
            Assert.AreEqual(tasks.Count(), expectedTasks);
        }

        [TestMethod]
        public async Task UpdateTaskAsync_ExpectedSameDescription()
        {
            // Arrange
            var testTask = this.tasksCollection.Where(t => t.IsDeleted != true).First();
            testTask.Description = "new description";

            // Act
            await this.taskService.UpdateTaskAsync(testTask);
            var updatedTask = await this.taskService.GetTaskAsync(testTask.Id);
            // Assert            
            Assert.AreEqual(testTask.Description, updatedTask.Description);
        }

        [TestMethod]
        public async Task DeleteTaskAsync_ExpectedDecrementTasksCount()
        {
            // Arrange
            var testTask = this.tasksCollection.Where(t => t.IsDeleted != true).First();
            var expectedTasksCount = this.tasksCollection.Count(t => t.IsDeleted == false) - 1;

            // Act
            await this.taskService.DeleteTaskAsync(testTask.Id);
            var updatedTasks = await this.taskService.GetAllTasksAsync(null);

            // Assert            
            Assert.AreEqual(expectedTasksCount, updatedTasks.Count());
        }

        [TestMethod]
        public async Task DeleteAllCompletedTasksAsync_Expected2TasksInDb()
        {
            // Arrange
            var expectedActiveTaskCount = this.tasksCollection
                .Where(t => t.IsCompleted == false && t.IsDeleted == false).Count();

            // Act
            await this.taskService.DeleteAllCompletedTasksAsync();
            var updatedTasks = await this.taskService.GetAllTasksAsync(null);

            // Assert            
            Assert.AreEqual(expectedActiveTaskCount, updatedTasks.Count());
        }
    }
}

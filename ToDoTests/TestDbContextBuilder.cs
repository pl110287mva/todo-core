﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using ToDo.DataAccess;
using ToDo.DataAccess.Models;

namespace ToDoTests
{
    class TestDbContextBuilder
    {
        private readonly ToDoContext context;
        public TestDbContextBuilder()
        {
            var options = new DbContextOptionsBuilder<ToDoContext>()
                .UseInMemoryDatabase(databaseName: "TestDB")
                .Options;

            this.context = new ToDoContext(options);
        }

        /// <summary>
        /// Fill in db with test values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public bool FillDb(IEnumerable<AppTask> tasks)
        {          
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            return true;
        }

        /// <summary>
        /// Get filled db.
        /// </summary>
        /// <returns></returns>
        public ToDoContext BuildContext()
        {
            return this.context;
        }
    }
}
